# Web_Authenticator
This is the web based google authenticator

Follow the video.

Run the `db.py` to create a db if not exist.

- Clone the repository using `git clone`
- Change the directory to newly cloned repo
- Run the application using `Python Serv.py`
```bash

#Assuming user has root privilages
#Clone the repo
$ git clone https://github.com/zzduts/Web_Authenticator.git
#Change the directory "Web_Authenticator"
$ cd Web_Authenticator/
$ ls
# Install the flask library using pip
$ pip install flask
#Find the Serv.py file and start the server with the following command
$ python Serv.py
#This will be running in Port 80 and Front end is rendered
```
> Note: Make sure `Python` is installed before running the program.

Team Reborninfosec

Reference: https://stackoverflow.com/questions/8529265/google-authenticator-implementation-in-python
