from flask import Flask, render_template, request
import sqlite3 as sql
import json
import totp
app = Flask(__name__)


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/enternew')
def new_student():
    return render_template('add_new.html')


@app.route('/addrec', methods=['POST', 'GET'])
def addrec():
    if request.method == 'POST':
        try:
            name = request.form['site_name']
            sec = request.form['site_secret'].replace(" ", "")
            print sec

            with sql.connect("database.db") as con:
                cur = con.cursor()

                cur.execute(
                    "INSERT INTO totp (site, secret) VALUES(?, ?)", (name, sec))

                con.commit()
                msg = "Record successfully added"
        except:
            con.rollback()
            msg = "error in insert operation"

        finally:
            return render_template("result.html", msg=msg)
            con.close()


def make_dicts(cursor, row):
    return dict((cursor.description[idx][0], value)
                for idx, value in enumerate(row))


@app.route('/list', methods=['GET'])
def list():
    con = sql.connect("database.db")
    con.row_factory = sql.Row

    cur = con.cursor()
    cur.execute("select * from totp")

    items = cur.fetchall()
    val = []
    for item in items:
        tup = ()
        for k in range(0, len(item)):
            if k % 2 == 1:
                tup = tup + (totp.get_totp_token(str(item[k])),)
            elif k % 2 == 0:
                tup = tup + (str(item[k]),)
        val.append(tup)
    # return json.dumps(dict(val)) + '\n <br><a href = "/" > Go back to home page < /a ></br>'
    return render_template("list.html", rows=val)


if __name__ == '__main__':
    app.run(debug=True)
